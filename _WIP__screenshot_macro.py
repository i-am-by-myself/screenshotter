# -*- coding: utf-8 -*-
from PIL import Image, ImageGrab
from pynput.mouse import Button, Controller
import pyperclip
import keyboard
import pyautogui
import time

mouse = Controller()

def Screenshotter(bbox, sav_dir, num_bbox = 1, image_num = 1): #For having multiple bounding boxes in a screen
    """
    Parameters
    ----------
    bbox : Bounding box for the screenshot.
        INPUT :(left bound, upper bound, right bound, lower bound).
    sav_dir : Directory to save the screenshotted images. Numbers images
    from initial to the final image screenshotted.
        INPUT : folder to save at.
    num_bbox : (Optional) number for bounding boxes if multiple are needed.
        INPUT : integer number 1 or above (Default is 1).
    image_num : Current count of images done.
        INPUT : integer number 1 or above (Default is 1).
    Returns
    -------
    number of images done so far.
    """
    for i in range(num_bbox):
        picture = ImageGrab.grab(bbox=(int(bbox[i][0]),int(bbox[i][1]),int(bbox[i][2]),int(bbox[i][3])))
        picture.save(sav_dir + str(image_num) + ".PNG", "PNG")
        image_num += 1
    return image_num
        

def macro(next_page, mouse_pos): #Next page macro
    mouse.position = mouse_pos
    mouse.press(Button.left)
    mouse.release(Button.left)
    for i in range(4):
        pyautogui.press('backspace')
        pyautogui.press('delete')
    list_number = [int(x) for x in str(next_page)]
    for i in range(len(list_number)):
        pyautogui.press(str(list_number[i]))
    keyboard.press_and_release('enter')

def copy_number(mouse_pos):
    mouse.position = mouse_pos
    mouse.press(Button.left)
    mouse.release(Button.left)
    pyautogui.hotkey('ctrl', 'a')
    pyautogui.hotkey('ctrl','c')
def check_progress(mouse_pos, last_num): #Checks progress by copying and comparing against a given value
    #pyautogui library implementation    
    copy_number(mouse_pos)
    time.sleep(0.1)
    num = pyperclip.paste()
    done = (int(num) >= last_num) and (not (int(num) > 899 and int(num) < 1000))
    return (done)

def convert_images_pdf(folder_dir, first_num, last_num): #method 3(without img2pdf)
    imList = list()
    im1 = Image.open(folder_dir + '/' + str(first_num) + '.png')
    for i in range(first_num + 1,last_num + 1): #Adds images to the list
        im = Image.open(folder_dir + "/" + str(i) +".png")
        imList.append(im)
    im1.save(folder_dir + "/pdf.pdf", save_all = True, append_images=imList)
    
def create_bbox(bbox, bbox_num = 0):
    def another_bbox(bbox, bbox_num):
        more = input("Another Box? (Input only Y and N): ")
        more = more.upper()
        while more != "Y" and more != "N":
            more = input("Enter the correct letter: ")
            more.upper()
        if more == "Y":
            bbox = create_bbox(bbox, bbox_num)
        return [bbox, bbox_num]
    more = "Y"
    while more == "Y":
        bbox_num += 1
        bbox_temp = input("Enter bounding box(left,upper,right,lower): (too lazy to check for errors. Input correctly) ")
        bbox_temp1 = bbox_temp.split(",")
        bbox.append(bbox_temp1)
        print(bbox_num)
        more = input("Another Box? (Input only Y and N): ")
        more = more.upper()
        while more != "Y" and more != "N":
            more = input("Enter the correct letter: ")
            more.upper()
        
    return [bbox, bbox_num]
    
if __name__ == "__main__":
    x = 0
    bbox = []
    num_bbox = int
    return_create_bbox = create_bbox(bbox)
    num_bbox = return_create_bbox[1]
    bbox = return_create_bbox[0]
    first_num = int(input("1st page number: "))
    last_num = int(input("Last page number: "))
    save_dir = input("directory to save files (use /): ")
    if save_dir[-1] != "/":
        save_dir = save_dir + "/"
    mouse_pos = input("position inside text box: ")
    mouse_pos = mouse_pos.split(",")
    time.sleep(5) #To let user switch to their web browser
    for image_num in range(first_num, int(last_num)):
        for i in range(num_bbox):
            picture = ImageGrab.grab(bbox=(int(bbox[i][0]),int(bbox[i][1]),int(bbox[i][2]),int(bbox[i][3])))
            picture.save(save_dir + str(image_num) + ".PNG", "PNG")
            image_num += 1
        check_progress(mouse_pos, last_num)
        macro(image_num + 1, mouse_pos)
        time.sleep(10) #To let next page load. 10 sec for sanity RN
    convert_images_pdf(save_dir, first_num, last_num)